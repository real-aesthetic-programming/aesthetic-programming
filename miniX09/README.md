# MiniX09

## Flowchart for “Dress Me Up!”

Run the code  [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX06/index.html)  <br/>

See the code  [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX06/miniX6.js)  <br/>

![enter image description here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX09/flowchart.png?ref_type=heads)

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

In creating the flowchart for my dress-up game, I grappled with the challenge of simplifying the code into the form of a flowchart while retaining the real complexity of all the functionalities in my code. The difficulty lies in simplifying the intricate procedures into a human-readable step-by-step diagram to visually represent my game, but at the same time without oversimplifying all the different elements of the code. I tackled this challenge through hierarchical decomposition, layered abstraction, and iterative refinement - with these strategies, the aim was to strike a balance between clarity and completeness, ensuring that the flowchart effectively communicates the game's procedural logic while still holding onto the readability and simplicity of the flowchart form.

**In which ways is the flowchart you produced useful?**

The flowchart helped to break down one of my most complex MiniXs yet, which in turn helped me understand the core functionality of it better. Flowcharts in this way help bridge the gap between people with coding experience, and those without, and does not exclude anything from either party.

## References

Soon Winnie & Cox, Geoff, “Auto-generator”, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-225 - chapter 9.

