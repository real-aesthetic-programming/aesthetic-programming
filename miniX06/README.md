# MiniX06

## “Dress Me Up!”

Run my code  [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX06/index.html)  <br/>

See my code  [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX06/miniX6.js)  <br/>

![Dress Me Up title screen](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/miniX6_screenshot.png)

![Dress Me Up play screen](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/miniX6_screenshot2.png)

[Watch Video of Gameplay](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/Dress_Me_Up_Video.mp4)

**Describe how does/do your game/game objects work?**

In my game, the main objects consist of a blank character with clothes around them to dress them up with. Firstly, the player is met with a title screen that also consists of two bobbing objects, the title and a play button. After clicking the play button the player sees a new interface with the character and clothing. The way the game works is by clicking on the clothing items you wish for the character to wear, and they appear right on the model.

In my code, the clothing objects are organised in a class called Wardrobe and are each styled with specific z-indices to ensure they appear correctly in relation to one another when worn (like the shoes appear over the socks, but underneath the pants). I also implemented some further customisation with a "tattoos!" button that adds tattoos to the character if desired, and takes them off too. To remove the clothes and build a new outfit, the player can simply click on the character until they are bare again.

**Describe how you program the objects and their related attributes, and the methods in your game**

Within the class Wardrobe, which all the clothing and accessories are instances of, I added one method called `toggleVisibility` which is applied after a `mousePressed` event, that checks whether the clothing is visible on the character or not, which is initialised in the class as false using `this.visible = false`, since at the start of the game, the character has no clothes on.  Then when a clothing item is placed on the character, `this.visible = !this.visible` - so "true" - and so when the method checks whether the clothing item is placed on the character or not, it will be true this time, and then the item can be removed on a `mousePressed` event.

As for their attributes, the all instances have a an image, the same size, a starting position, a target position and a z-index, all defined in the constructor method - as well as the `this.visible = false` attribute from the start.

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming (OOP) is a software design approach that emphasizes the organization of code into manageable units called objects. These objects encapsulate both data and the operations/methods that can be performed on that data. Through concepts like inheritance, where objects can inherit characteristics from other objects, and encapsulation, which hides the internal workings of objects, OOP promotes code reuse, modularity, and maintainability. It's like assembling building blocks, where each block represents a specific piece of functionality, making the overall structure of the code more understandable and adaptable.

Abstraction involves focusing on the essential aspects of a system while hiding unnecessary details. It allows developers to work with higher-level concepts without being bogged down by implementation specifics. By abstracting away complexities, abstraction simplifies code comprehension and modification, making it easier to manage and extend over time. This approach fosters collaboration among developers by providing a clear and concise representation of the software's functionality. Ultimately, OOP and abstraction together enable more efficient and error-resistant software development.

## References

Soon Winnie & Cox, Geoff, “Auto-generator”, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 6.  

[loadSound p5 reference](https://p5js.org/reference/#/p5/loadSound)<br/>

[BeepBox - music making website](https://www.beepbox.co/#9n31s0k0l00e03t2ma7g0fj07r1i0o432T7v1u71f50p61770q72d42g3q0F21a90k762d06HT-SRJJJJIAAAAAh0IaE1c11T1v1u40f0qwx10r511d08A4F2B6Q0068Pf624E2b676T5v1ua4f62ge2ec2f02j01960meq8141d36HT-Iqijriiiih99h0E0T4v1uf0f0q011z6666ji8k8k3jSBKSJJAArriiiiii07JCABrzrrrrrrr00YrkqHrsrrrrjr005zrAqzrjzrrqr1jRjrqGGrrzsrsA099ijrABJJJIAzrrtirqrqjqixzsrAjrqjiqaqqysttAJqjikikrizrHtBJJAzArzrIsRCITKSS099ijrAJS____Qg99habbCAYrDzh00E0b4h400000000h4g000000014h000000004h400000000p16000000) <br/>

[sin p5 reference](https://p5js.org/reference/#/p5/sin) <br/>
