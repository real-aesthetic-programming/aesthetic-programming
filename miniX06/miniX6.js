class Wardrobe {
  constructor(imgURL, startX, startY, zIndex) {     // each Wardrobe instance has to have an imageURL, a starting position and a z-index (so that i can control which clothes appear on top of which)
    this.img = createImg(imgURL)                    // assigning the image URL as the instance image
    this.img.size(245, 520)                         // has to be same size as blankModel
    this.img.position(startX, startY)               // determining the starting position with the constructor arguments startX and startY
    this.img.style('z-index', zIndex)               // applying the z-index (CSS)
    this.zIndex = zIndex                            // Store z-index value
    this.startX = startX                            // removing this would make it so the clothes cannot be removed again and revert to their starting positions
    this.startY = startY 
    this.targetX = 650                              // same target position for all instances because of the way i drew them, they align at the same origin :)
    this.targetY = 30
    this.visible = false                            // initialize as not visible
    
    this.img.mousePressed(() => {                   // adding event listener for mouse click
      this.toggleVisibility()                       // if the image is pressed, apply the toggleVisibility method
    })
  }

  toggleVisibility() {
    if (this.visible) {
      this.img.position(this.startX, this.startY)   // move to starting position
    } else {
      this.img.position(this.targetX, this.targetY) // move to target position
    }
    this.visible = !this.visible                    // toggle visibility
  }
}

let titleBackgroundImg                              // variable to store the title background image element
let gameTitle                                       // more variables to store images vvv
let playButton
let playBackgroundImg
let angle = 0                                       // angle for sine wave motion (for the bobbing effect on the title screen)
let gameState = "title"                             //variable to keep track of what state of the game we're in, title or play
let bgMusic
let blankModel

let tattoosButton                                   // declaring the variable for the tattoos button
let tattoosVisible = false                          // variable to keep track of tattoos visibility
let tattooImage                                     // variable to store the tattoo image element

function preload() {
  bgMusic = loadSound('DressMeUpTheme.mp3')         // load the background music
}


function setup() {
  createCanvas(1530, 690)                           // creating canvas the same size as the background image (which is roughly the window's size on Chrome)
}


function draw() {
  if (gameState === "title") {                      // checking which game state the program is at, and displaying the correct screen
    drawTitleScreen()
  } else if (gameState === "play") {
    drawPlayScreen()
  }
}


function drawTitleScreen() {
  // create image elements
  titleBackgroundImg = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Background.png?ref_type=heads")
  titleBackgroundImg.position(0, 0)


  gameTitle = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Dress%20me%20up%20text.png?ref_type=heads")
  gameTitle.size(700, 380)


  playButton = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Button2.png?ref_type=heads")
  playButton.size(250, 250)


  let verticalBobbingOffset = sin(angle) * 10          // adjust the amplitude of the bobbing
  // update angle for sine wave motion
  angle += 0.1                                         // adjust speed of the bobbing motion
  // update positions of image elements with bobbing motion
  gameTitle.position(150, 200 + verticalBobbingOffset) // apply bobbing motion vvv
  playButton.position(1000, 250 + verticalBobbingOffset)
}


function drawPlayScreen() {
  // create image elements
  playBackgroundImg = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/yellow_background.png")
  playBackgroundImg.position(0, 0)
  playBackgroundImg.size(1530, 690)


  blankModel = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Purple%20Body.png")
  blankModel.size(245, 520)
  blankModel.position(650, 30)

  tattoosButton = createButton("")                    // creating a blank button that will have an image as the background
  tattoosButton.position(700, 20)
  tattoosButton.size(120, 30)
  tattoosButton.style('background-image', 'url("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/tattoos__button.png")') // set button background image
  tattoosButton.style('background-size', 'cover')
  tattoosButton.style('z-index', '1000')

  tattoosButton.mousePressed(toggleTattoos)          // add event listener for mouse clicks on the tattoos button

  // creating new instances in the class Wardrobe:
  let greenHoodie = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Green%20hoodie.png?ref_type=heads", 20, 300, 100)
  let TFhoodie = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/TF%20hoodie.png?ref_type=heads",250, 300, 100)

  let dungarees = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Dungarees.png", 450, 50, 75)

  let blueJeans = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Blue%20jeans.png?ref_type=heads", 1000, -260, 35)
  let darkJeans = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Dark%20jeans.png?ref_type=heads", 1160, -260, 35)
  let lightJeans = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Light%20jeans.png?ref_type=heads", 1320, -260, 35)
  let shorts = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Shorts.png", 825, -260, 50)

  let floralSkirt = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Floral%20skirt.png", -30, -120, 38)
  let dress = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Green%20dress.png", 150, 0, 33)
  
  let blueTee = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Blue%20tee.png?ref_type=heads", 900, 220, 60)
  let greenTee = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Green%20tee.png?ref_type=heads", 1100, 220, 60)
  let hawaiianShirt = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Hawaiian%20shirt.png?ref_type=heads", 1300, 220, 60)

  let glasses = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Glasses.png", 750, 520, 73)
  let blueHat = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Blue%20Hat.png", 910, 570, 75)
  let pinkHat = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Pink%20Hat.png", 1100, 570, 75)
  let headphones = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Headphones.png",1290, 550, 74)

  let whiteTop = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/White%20tube%20top.png?ref_type=heads", 450, 400, 50)
  let orangeTop = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Orange%20tube%20top.png?ref_type=heads",590, 400, 50)

  let socks = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Orange%20socks.png", 305, -200, 31)

  let flipFlops = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Flip%20flops.png", -30, -430, 39)
  let whiteSneakers = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/White%20sneakers.png", 340, -350, 34)
  let redSneakers = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Red%20n%20black%20sneakers.png", 340, -430, 34)
  let boots = new Wardrobe("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/Boots.png", 150, -400, 34)
}


function mousePressed() {
  if (gameState === "title") {
    if (                                                     // checking if the mouse is over the play button
      mouseX > playButton.position().x &&
      mouseX < playButton.position().x + playButton.width &&
      mouseY > playButton.position().y &&
      mouseY < playButton.position().y + playButton.height
    ) {
      gameState = "play"                                     // change game state to play
      getAudioContext().resume()                             // this is necessary for the music to play, cuz of browser rules
      bgMusic.loop()                                         // start playing the theme on a loop
    }
  }
}

function toggleTattoos() {
  if (tattoosVisible) {
    tattooImage.remove()                                     // remove tattoo image if visible
  } else {                                                   // create and position the tattoo image
    tattooImage = createImg("https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX06/allPNGs/All_Tattoos.png")
    tattooImage.position(650, 30)
    tattooImage.size(245, 520)                               // same as the clothes and blankModel
    tattooImage.style('z-index', '30')
  }
  tattoosVisible = !tattoosVisible                           // toggle tattoos visibility
}
