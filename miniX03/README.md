# MiniX03

## "Real Life"  

Run my code [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX03/index.html) <br/>
See my code [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX03/miniX3.js) <br/>

![Screenshot](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX03/Screenshot.png)

***What do you want to explore and/or express?***

To create my throbber, I used the sample code written in the Aesthetic Programming book on page 82, and basically did the exersise written below it which said to play around with the values and see what affect the changes have. I played around with the sample code for quite a bit until I achieved something a little trippy and very far from the original rotating circles. Firstly, I adjusted the variable `num` to create less space between the generated ellipses in the animation. I then messed with the dimensions of the `ellipse` function to a point where it hardly renders ellipses anymore, and looks more like lines instead. Then I added a few more static lines than the original code had, changed the colours of everything to a light green, and it all looked so Matrix-like so I fully dove in to that universe and added some text and a Neo.png for good measure.

I implemented a `for` loop to create a grid made up of lines for the background, so it would look more matrix-like.

I liked the idea that a simulation as powerful and advanced as the Matrix would still need some time to load, just like the software and "simulations" (games) we anxiously wait for today.

***Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?***

To be hyperbolic, I don't think anything infuriates me more that the YouTube throbber. Every time, without fail, a surge of rage runs through my whole body at the sight of it - it's incredible how influencial some little grey circles can be. It's like suddenly I am being deprived of life, something is being taken away from me and there's not much I can do to make it go away faster. In terms of what I think this throbber is communicating to me, I wouldn't say this specific one communicates progress, I think it's saying the opposite actually. It acts like more of a roadblock - once you see it, you can only go back where you came from. In terms of what it potentially is hiding, I think it is hiding the fact that nothing is truly happening most of the time. The looping animation works as a false sense of progress when in reality, nothing is being done.

## References  

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96 (Chapter 3)<br/>