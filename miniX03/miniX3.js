function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);                                  //Number of frames per second
}
function draw() {
  background(20, 12);
  drawElements();                                 //Function created underneath, called here.
  let img = createImg("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/1dbc1935-6542-4ee3-822f-135cff4ba62c/dcvfx92-0ccf9181-f1cb-4509-993e-8774dcacef34.png/v1/fill/w_1024,h_1328/the_matrix__neo___transparent__by_speedcam_dcvfx92-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTMyOCIsInBhdGgiOiJcL2ZcLzFkYmMxOTM1LTY1NDItNGVlMy04MjJmLTEzNWNmZjRiYTYyY1wvZGN2Zng5Mi0wY2NmOTE4MS1mMWNiLTQ1MDktOTkzZS04Nzc0ZGNhY2VmMzQucG5nIiwid2lkdGgiOiI8PTEwMjQifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.4uLLKaAaEFEyyV0IzZY_Cd2toKFcQCH5be5DY3RNBNA")
  img.position(900, 40)
  img.size(750, 900)

}
function drawElements() {
  let num = 90;                                   //This means that the elements drawn in the drawElements() function will be rotated 360 degrees divided by 90, resulting in 4-degree increments for each element
  push();                                         //Save the current position and orientation settings
  translate(width / 2, height / 2);               //Move things to the center
  let cir = 360 / num * (frameCount % num);       //360/num is the degree of each ellipse's movement, this line calculates the angle of rotation based on frameCount
  rotate(radians(cir));                           //Rotate the canvas by the calculated angle
  noStroke();
  fill(100, 255, 100);

  ellipse(50, 0, 2, 5000);                        // The x parameter is the ellipse's distance from the center
  pop();                                          // Retrieve the previously saved position and orientation settings
  stroke(100, 255, 100, 2);                       // Set the stroke color with an alpha value for transparency

  for (let i = 60; i <= width - 240; i += 60) {
    line(i, 0, i, height);                        // Line grid
    line(0, i, width, i);
  }

  textSize(22);                                   //Text
  fill(255);
  strokeWeight(5);
  text("you'll be brought back to your real life in just a moment...", 120, 120);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);        //Resizes canvas to match new window dimensions
}