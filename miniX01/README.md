# MiniX01
## "Never Stop"
Run my code [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX01/index.html)<br/>
See my code [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX01/miniX1_never_stop.js?ref_type=heads)<br/>

![initial webpage](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX01/miniX01_screenshot_1.jpg)
![webpage after some time](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX01/miniX01_screenshot_2.jpg)
![webpage after some more time](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX01/miniX01_screenshot_4.jpg)

***What have you produced?*** <br/>
For my first miniX I wanted to play with `for` loops and randomisation in order to acheive a webpage that is unique every time. I landed on an idea with googly eyes which I then realised was probably due to the film "Everything Everywhere all at Once" that has subconsiously inspired me to put googly eyes on everything I possibly can. Therefore, I wanted to plaster a webpage with as many googly eyes as I could imagine, and with the help of `for` loops I was able to achieve that.

The code is rather short and simple, I started off creating an empty array called `images` as a container for the googly eyes that will be generated while the `draw()` function runs. This array is what the `for` loop then iterates through to render the googly eyes. These googly eyes are added into the array using the `images.push` method at the end of my code - but I'm getting ahead of myself.

In the `setup()` function I created my canvas that fit the window's dimensions and set the colour of it to black. Then inside the `draw()` function I added various text elements placed around the canvas to add something else to look at while the googly eyes gradually appear. As well as the text, my `for` loop and `if` statement were also written in the `draw()` function.

The `if` statement and `for` loop work together to generate the randomly placed googly eyes on the canvas. Firstly, the `if` statement checks if a randomly generated number is less than 0.02, giving a 2% chance per frame for a new googly eye to appear. If this condition is met, a new image object representing a googly eye is created with random size and position using the `createImg()` function, then, as mentioned, this image is added to the array `images`, which stores references to all the images on the canvas. In the `draw()` function, the `for` loop iterates over the `images` array, rendering each image at its randomly specified position. This process repeats continuously, resulting in a unique display of googly eyes appearing at random over time.

***How is the coding process different from, or similar to, reading and writing text?*** <br/>
The coding process is just like the process of learning to both read and write in any language. In the beggining it is frustrating to have to keep refering to a dictionary and not being able to express yourself freely with the limited vocabulary you have. I had so many ideas that ultimately were too ambitious for the level I am currently at with P5.js, which was a little discouraging to me as someone who finds the beginning stages of learning quite unbearable. However, ultimately it is like learning any skill, it takes a LOT of practice and a LOT of self-discipline too.

To compare coding to writing, I want to say it is wildly different from any language I've ever written in. Coding has a purpose of instructing, it has many specific rules for it to be able to function at all, whereas writing can take many forms like poetry, prose and journalism, to name a few, and even when written grammatically incorrect we can still make sense of it - this highlights a key difference in audience, code is written for a machine, whereas poetry is usually written for living beings. However, when it comes to writing a recipe or a set of instructions, the two don't differ as much. Both become ways of writing step by step, specific instructions  that if written wrong or misunderstood, will result in failure. In this specific case, both coding and writing have a certain expected outcome.

As for comparing reading code to reading text in a 'natural language', I don't think there is too much difference in the way one goes about reading and comprehending the two. It's all about learning the rules of syntax - understanding what goes where and what meaning that has. I have learned to read in 3 'natural languages', all 3 quite different from each other syntactically, and now learning to read code is much of the same. It's learning the vocabulary, the conjugations, the syntax and making sense of it all in a context. The way that in English the adjective goes before the noun, to me is the same as the fact that in Javascript the function goes before the arguments.

## References
[P5js.org "for" reference](https://p5js.org/reference/#/p5/for)<br/>
[P5js.org "text" reference](https://p5js.org/reference/#/p5/text)<br/>
[P5js.org "random" reference](https://p5js.org/reference/#/p5/random)<br/>
[The Coding Train - Random Circles (using for loop)](https://www.youtube.com/watch?v=XATr_jdh-44&ab_channel=TheCodingTrain)<br/>