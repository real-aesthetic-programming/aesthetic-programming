let images = []                                                 // this creates an empty array called images. this array will be used to store references to image objects
                                                                // without this array, there would be no structured way to keep track of the images that are currently on the canvas

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  textSize(20)
  fill(255)
  text('wait', 1000, 100)
  text('patience', 250, 200)
  text('slow down', 400, 600)
  text('look around', 900, 400)
  text('never stop', 700, 300)
  text('they will keep coming', 1000, 600)

  for (let i = 0; i < images.length; i++) {                     // loop will continue as long as "i" is less than the length of the images array
                                                                // i++ increments the loop counter variable i by 1 after each iteration
    const img = images[i];                                      // declares a constant variable "img" and assigns it the value of the image object retrieved from the images array at index i
                                                                // this reference allows me to perform operations on that specific image object inside the loop
    image(img, img.position.x, img.position.y);
  }

  if (random() < 0.02) {                                        // randomly generates number between 0 and 1 and checks if it is below 0.02
    const img = createImg("https://i.imgflip.com/5xb240.png");  // if yes (2% chance of happening) create the googly eye image
    img.size(random(25, 150), random(25, 150));                 // sets the size of the new image to a random width and height between 25 and 150 pixels
    img.position(random(width), random(height));                // generating random values within the width and height of the canvas
    images.push(img);                                           // the newly created image (img) is pushed into the images array, adding it to the list of images to be displayed
  }
}