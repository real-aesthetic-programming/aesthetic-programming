# MiniX02  

## "Emoji"  

Run my code [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX02/index.html)<br/>
See my code [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX02/miniX2.js?ref_type=heads)<br/>

![First Screenshot](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX02/Screenshot_1.png)

![Second Screenshot](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX02/Screenshot_2.png)

***Describe your program and what you have used and learnt.***  
I have created a very simple webpage with just the use of text, a simple interaction component and an emoji created using various shapes in the p5js library. For the text, I wanted to achieve a typewriter effect to make the webpage into more of an experience and less of a digital postcard. I looked into ways other p5js users had acheived this effect online but ultimately their ways of doing it were confusing to me as a beginner, so I resorted to ChatGPT to see if it could explain to me how to create this effect more simply. I ended up using an `if` statement with the `.length` and `.charAt` methods like this:

        if (textTyped.length  <  fullText.length) {
    textTyped  +=  fullText.charAt(textTyped.length);
    }

`textTyped` being an empty string, and `fullText` being the full string value that I wanted to have the typewriter effect be applied to.

As for the emoji part of the webpage, I am fully aware I haven't adhered to the requirements of the assignment, and have done this on purpose. I have only created one emoji for my webpage, instead of the required two, since making only one made the most sense for the message I want to convey. If it really is necessary, here's an emoji I made in the p5js web editor:
![Extra Emoji](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX02/extra_emoji.png)

Anyways, to create the watermelon emoji, I used the `arc` function and the `ellipse` function. I learned how to draw arcs from the p5js reference page and went from there, creating three arcs on top of each other, scaled down to create the three layers of the watermelon midsection - the green and white parts of the rind, and the red part of the fruit. The `ellipse` function was used to create the black seeds of the watermelon.

For the small interaction part of the webpage, I created a `mousePressed` function that would reveal the watermelon emoji and the text "Free Palestine" when someone clicks anywhere on the page.

***How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?***  

For this MiniX I was very stumped for a while, I did not know what I wanted to create with those silly yellow faces that would in any way touch on a societal issue. Then a while later, I "remembered" that emojis are way more than just the faces, there's an emoji for almost everything and they still keep coming. So I looked beyond the faces section and looked at the items. At first glance, the emoji items hold a surface level meaning, but some of them stick out because the development of emoji usage has made them into something else, take the eggplant emoji `🍆` for instance - you're lying to yourself if you see an eggplant anymore.

That landed me on the ever so relevant reinterpretation of the watermelon emoji. Originally used as a symbol of the summertime perhaps, now a symbol of support for Palestine and disdain of Isreal. The watermelon emoji is not something I would see on the internet before October 7th 2023, but is now used as an indicator, almost like a verification symbol, to show one's objection to the genocide of Palestinians. With my MiniX I wanted to show how emoticons can be reimagined and be used as powerful tools to show solidarity and resistance which transcend language barriers.

## References  

[P5js.org "arc" reference](https://p5js.org/reference/#/p5/arc) <br/>
[ChatGPT](https://chat.openai.com/) <br/>