let textTyped = "";                                             // Empty string value
let fullText = "Emojis can be quite silly, often not really meaning much.        \nHowever, they can become powerful symbols of solidarity.        \nClick to reveal.";
let revealWatermelon = false;
let allTextTyped = false;                                       // Variable to check if all text is typed

function setup() {
  createCanvas(550, 550);
  textSize(20);
  frameRate(12);                                                // Adjust typing speed
}

function draw() {
  background(220);

  text(textTyped, 25, 100);                                     // Displays the current content of textTyped
  
  if (textTyped.length < fullText.length && !allTextTyped) {    // checks if the length of the typed text is less than the length of the full text, if yes, it means there are still characters to be added
    textTyped += fullText.charAt(textTyped.length);             // Add a new character only if all text is not typed yet
  } else {                                                      
    allTextTyped = true;                                        // Else block after the if statement sets the allTextTyped value to true
  }

  if (revealWatermelon) {                                       // Display watermelon slice after mouse is pressed when the text is finished being written
    noStroke();
    fill(20, 100, 50);
    arc(275, 250, 144, 144, 0, 60, PI + QUARTER_PI, CHORD);
    fill(255);
    arc(275, 248.8, 120, 120, 0, 60, PI + QUARTER_PI, CHORD);
    fill(255, 0, 0);
    arc(275, 248.8, 108, 108, 0, 60, PI + QUARTER_PI, CHORD);
    fill(0);
    ellipse(251, 256, 6, 12);
    ellipse(275, 259.6, 6, 12);
    ellipse(299, 264.4, 6, 12);
    ellipse(261.8, 283.6, 6, 12);
    ellipse(286.6, 287.2, 6, 12);

    text("Free Palestine", 213, 400)
  }
}

function mousePressed() {
  if (allTextTyped) {                                            // Only allow click if all text is typed
    revealWatermelon = !revealWatermelon;
  }
}
