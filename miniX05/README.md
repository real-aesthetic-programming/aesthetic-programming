  
# MiniX05

## “a much less inspired one”

Run my code  [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX05/index.html)  <br/>
  
See my code  [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX05/miniX5.js) <br/> 

![miniX5 gif](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX05/miniX5_gif.gif)

![miniX5 screenshot](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX05/screenshot_miniX5.png)

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

My generative program creates circles that appear at random x and y coordinates on the canvas with a 60% of appearing, per frame. Each circle generated has a random size and color with varying levels of transparency and the circles move randomly across the canvas, at random speeds - so over time, the page fills with differently coloured circles all existing according to randomised properties. These are quite simple rules but make for an effectively self-generating webpage that is easy to grasp and pleasant to look at.

The emergent behavior in this program arises from the "interaction" between the randomly generated circles and their movement patterns. Although each circle behaves independently, their collective motion creates patterns that are not explicitly programmed. These emergent patterns give the artwork its charm and sporadic vibe.

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

The circles in my program are generated automatically based on predefined rules, yet they exhibit a degree of autonomy in their movement and appearance. They appear to behave and move freely and independently while still being maintained within ranges of random values.

The chapter emphasizes the idea of relinquishing control to allow for emergent behavior to unfold. It encourages embracing unpredictability and embracing the outcomes that arise from generative processes. In my MiniX, although simple, I have embraced this by employing randomness into every aspect of the circle's creation. Due to this randomness, the probability that two circles will have the same placement, size, colour, opacity, velocity etc. is incredibly low, and using ChatGPT to help calculate an estimation, it came down to a 1 in 270,823,500 chance of that happening.

## References

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 5. <br/>

[The Coding Train - while loops and for loops](https://www.youtube.com/watch?v=cnRD9o6odjk&t=597s&pp=ygUWZ2VuZXJhdGl2ZSBhcnQgaW4gcDVqcw==) <br/>
  
