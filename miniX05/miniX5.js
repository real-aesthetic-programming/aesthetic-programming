let circles = [];                                    // an array to store the circles created

function setup() {
  createCanvas(1530, 690);
  noStroke();                                        // disable stroke for circles
}

function draw() {
  background(0);
  
  // if statement to randomly create new circles
  if (random(1) < 0.6) {                             // randomly create circles with a 60% probability
    let x = random(width);                           // generate a random x-coordinate within canvas width
    let y = random(height);                          // generate a random y-coordinate within canvas height
    let size = random(20, 70);                       // generate a random size between 20 and 70 pixels
    let shapeColor = color(random(255), random(255), random(255), random(100, 200)); // generate a random color with alpha (transparency) between 100 and 200
    circles.push(new Shape(x, y, size, shapeColor)); // add a new shape to the array
  }
  
  // move and display existing circles
  for (let i = circles.length - 1; i >= 0; i--) {    // loop that iterates over each shape in the circles array, but it starts from the last shape and goes backward to the first one
    circles[i].move();                               // move the shape
    circles[i].display();                            // display the shape
    if (circles[i].isOffscreen()) {                  // check if the shape is off-screen
      circles.splice(i, 1);                          // remove the shape from the array if it's off-screen
    }
  }
}

// shape class definition
class Shape { 
  constructor(x, y, size, color) {                   // initialisation function - sets initial properties of the shape
    this.x = x;                                      // x-coordinate of the shape
    this.y = y;                                      // y-coordinate of the shape
    this.size = size;                                // size of the shape
    this.color = color;                              // color of the shape
    this.speedX = random(-2, 2);                     // random horizontal speed
    this.speedY = random(-2, 2);                     // random vertical speed
  }

  // method to move the shape
  move() {
    this.x += this.speedX;                           // update x-coordinate based on horizontal speed
    this.y += this.speedY;                           // update y-coordinate based on vertical speed
  }

   // method to display the shape
  display() {
    fill(this.color);                                // set fill color to the random color
    ellipse(this.x, this.y, this.size);              // draw a circle at (x, y) with a given size
  }

  // method to check if the shape is off-screen
  isOffscreen() {
    return (this.x < -this.size ||                   // check if the shape is off the left side
            this.x > width + this.size ||            // check if the shape is off the right side
            this.y < -this.size ||                   // check if the shape is off the top
            this.y > height + this.size);            // check if the shape is off the bottom
  }
}
