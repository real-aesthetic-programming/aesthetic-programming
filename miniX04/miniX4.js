let mic;
let frequencySpectrum;                                            // will be used when mapping the frequency spectrum using the mic input

let capture;
let faceTracker;

let lat;
let lon;
let locationDenied = false;

let spectrumActive = false;                                       // variable to control spectrum activation

function setup() {
  createCanvas(1530, 690);
  noFill();

  mic = new p5.AudioIn();                                         // audio
  mic.start();
  frequencySpectrum = new p5.FFT();                               // FFT = fast fourier transform, the frequency spectrum shows the amplitudes at different frequency levels (of the mic input)
  frequencySpectrum.setInput(mic);

  capture = createCapture(VIDEO);                                 // video
  capture.hide();

  navigator.geolocation.getCurrentPosition(getPosition, handleLocationPermissionDenied); // GPS tracker
  
  faceTracker = new clm.tracker();                                // face tracker
  faceTracker.init(pModel);
  faceTracker.start(capture.elt);
}

function getPosition(position) {                                  // function that obtains the latitude and longitude of your device and assigns those values to the lat and lon variables
  lat = position.coords.latitude.toFixed(6);                      // correct to 6 decimal places
  lon = position.coords.longitude.toFixed(6);
}

function handleLocationPermissionDenied() {                       // function to handle if location tracking was denied used later to output a different message
  locationDenied = true;
}

function draw() {
  background(0);

  if (spectrumActive) {                                            // checks if spectrum is active (true) - this is 
    let spectrum = frequencySpectrum.analyze();                    // frequency spectrum analysis
    stroke(255);
    beginShape();
    for (let i = 0; i < spectrum.length; i++) {
      vertex(map(i, 0, spectrum.length, 0, width), map(spectrum[i], 0, 255, height, 0));
    }
    endShape();
  }

  textSize(16);
  textAlign(LEFT);

  push();                                                           // using push and pop to ensure the styling here doesn't affect the styling of the frequency spectrum
  textStyle(BOLD);
  strokeWeight(0);
  fill(255, 0, 0);
  if (locationDenied) {
    text("GIVE ME YOUR LOCATION", 600, 20);
  } else if (lat !== undefined && lon !== undefined) {
    text("Your Current Location", 600, 20);
    text("Latitude: " + lat, 600, 50);
    text("Longitude: " + lon, 600, 70);
  } else {
    text("Trying to fetch your location...", 20, 50);               //this appears when the user is not connected to the internet for example
  }
  pop();

  push(); //ASCII art and text
  strokeWeight(0);
  fill(255);
  text("$ press the spacebar to see your frequency spectrum $", 10, 460)
  text("                          s$              s\n                         ,s$$             s$\n                        ,s$$$            s$$\n                        .s$$$        ,   s$$ \n                       ,s$$$$       .s$   $$ \                  ,    $$$$$.      s$    $\n                  ,$   ,$$$$$$s     s$      \n                 ,s$    ,$$$$$$s   $$$\n                 ,$$     ,$$$$$$s.   $$s\n                ,$.     $$$$$$$s .s$$$    s\n                 ,$$.     $$$$$$$ $$$$   s \n                   ,$$s     ,$$$$$$s$$$   s$\n                    ,$$s    ,$$$$$s$$$$  s$$\n               ,s.  $$$$   ,s$$$$$$$$  .s$$   s\n                ,$$ s$$$$..s$$$$$$$$$$$$$$   s$\n                ,s$.s$$$$s$$$$$$$$$$$$$$$$ s$$\n               ,s$$$$$$$$$$$$$$$$$$$$$$$$$$$ \n              ,s$$$ssss$$$$$$$$$$$$$ssss$$$$$\n             ,$$s$$$$$$$$$s$$$$$$s$$$$$$$$s$,\n              ,$$$$$$$$$$$$$s$s$$$$$$$$$$$$s\n             $,$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n              ,$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n               ,$$$$$$$$$$$$$$$$$$$$$$$$$$$ \n                ,$$$$$$$$$$$$$$$$$$$$$$$$$ \n                 ,$$$$$$$$$$$$$$$$$$$$$$$ \n                   ,$$$$$$$$$$$$$$$$$$$ \n                     ,$$$$$$$$$$$$$$$ \n                       ,$$$$$$$$$$$ \n                          ,$$$$$ \n                            ,$ ", 550, 180);
  text("           __________                                     CA$H COW\n  .'----------`.                                 \n  | .--------. |                             \n  | |########| |       __________              \n  | |########| |      /__________\             \n.--------| `--------' |------|    --=-- |-------------.\n|        `----,-.-----'      |o ======  |             | \n|       ______|_|_______     |__________|             | \n|      /  %%%%%%%%%%%%  \                             | \n|     /  %%%%%%%%%%%%%%  \                            | \n|     ^^^^^^^^^^^^^^^^^^^^                            | \n+-----------------------------------------------------+\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ", 900, 100);
  pop();

  let facePositions = faceTracker.getCurrentPosition()              // this gets the point numbers of the face
  image(capture, 0, 0, 590, 440);                                   // this places the camera capture at a specific spot
  filter(POSTERIZE, 2);                                             // filter that minimises the amount of colours of the output, the higher the number the more colours
}

function mousePressed() {                                           // function that places dollar signs where and when the mouse is pressed (event listener function)
  let dollarSign = createImg("https://gallery.yopriceville.com/var/albums/Free-Clipart-Pictures/Money.PNG/Green_Dollar_Sign_PNG_Clipart.png?m=1629808125")
  dollarSign.position(mouseX - 15, mouseY - 20);                    // Centering the dollar sign image on mouse position instead of top left corner
  dollarSign.size(30, 40);
}

function keyPressed() {
  if (key == ' ') {                                                 // Check if spacebar is pressed
    getAudioContext().resume();

    spectrumActive = !spectrumActive;                               // Toggle frequency spectrum
  }
}
