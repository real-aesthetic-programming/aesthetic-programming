# MiniX04
  

## "you.product"   
  

Run my code [here](https://aesthetic-programming-real-aesthetic-programming-6b4dc7719a700b.gitlab.io/miniX04/index.html)  <br/>  
See my code [here](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/blob/main/miniX04/miniX4.js?ref_type=heads)  <br/>    

![miniX4 gif](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX04/MiniX4_Gif.gif?ref_type=heads)

![miniX4 screenshot](https://gitlab.com/real-aesthetic-programming/aesthetic-programming/-/raw/main/miniX04/miniX4_screenshot.png?ref_type=heads)

***Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.***  

My MiniX titled **you.product** aims to depict what every user online is to a computer: stats, figures and inputs. I chose a dark colour theme to depict a negative (negative like inverted - but pun intended) form of the usual black text on white background we encounter quite often on the web. With this MiniX I wanted to display various forms of human and device data that websites can be tracking and dumped them all in one place to depict the abundance of what we're "selling" online. I took major inspiration from the line "if you are not paying for a product, you are the product" so I used many money "$" symbols throughout my code to signify the fact that everything that can be monetised IS monetised.

***Describe your program and what you have used and learnt.***  

My program captures webcam input and microphone input, as well as the device's coordinates (location). I felt that this specific combination of data could be displayed in an interesting way that could bring forth a message of private data and how easily it can be taken and manipulated to show on a webpage. I played around with filters for the webcam and landed on one called "Posterize" which is a filter that limits the amount of colours of the camera output. This filter can be manipulated with an extra value that determines the amount of colours that are shown (the higher the number the more colours).

To get the coordinates of the device I found some rather simple code online that used this: `navigator.geolocation.getCurrentPosition(getPosition)` which alongside the function `getPosition` could get a pretty accurate placement of the device using latitude and longitude which I chose to display next to the webcam capture. When you try the code for yourself you can write the coordinates into Google Maps and see just how accurate it is.

As for the microphone data and how I chose to visualise it on the webpage, I wanted to make a frequency spectrum across the page that graphs what the microphone picks up and shows the amplitudes at different frequency levels (low to high). In p5 this is achieved by creating an FFT - Fast Fourier Transform - which is an algorithm used to transform a signal from its original domain (time or space usually) to the frequency domain, revealing the frequency components of the signal. Basically, with an FFT algorithm I was able to create a spectral analysis of the microphone input - I found this out through p5's own website referenced below. I had a lot of problems with this feature when wanting to run it live on Chrome as it would not let me access the microphone (even after "allowing" it) and just displayed a straight white line instead of the frequency spectrum. I figured out through looking at the console that browsers now have this feature that do not allow audio to be played or picked up without user interaction to initiate it. I tried to fix the issue by making the frequency spectrum only appear when the spacebar is pressed but the problem remained the same (I also tried to create a button that would activate it but it was to no avail). Therefore, the frequency spectrum was only visible to me when running the code "live" from VS code and then disappeared after refreshing the page... :(

UPDATE: I used `getAudioContext().resume();` in my `keyPressed` function and now it works!

For the aesthetics I added some examples of ASCII art to the page, one made up of primarily the $ symbol for reasons I mentioned above, and one that I purposefully messed up that was originally a laptop on a desk but I thought it would look cooler and fit the general feel of the page if it was wonky.

The final feature I added was a `mousePressed` function that would create a dollar sign image at the mouse's position, when clicked.

***What are the cultural implications of data capture?***  

I think one main implication of data capture is that it feeds into the prevalent surveillance culture where people feel constantly observed and analysed in every way, especially online but also in real life. I think this can have implications on freedom of expression, dissent, and individuality, as people may feel too exposed and scrutinised knowing that they are constantly being monitored and ultimately "collected" like tokens.

## References   
  
Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, Chapter 4 <br/>  
[Frequency Spectrum p5js](https://p5js.org/examples/sound-frequency-spectrum.html) <br/>
[The Coding Train - Sound Visualisation in p5](https://www.youtube.com/watch?v=2O3nm0Nvbi4)<br/>
[Posterize Filter](https://editor.p5js.org/bclo/sketches/nRutEeH4_)<br/>
[GPS Location Tracker p5js](https://editor.p5js.org/reachamaatra/sketches/p0HMlPu65)<br/>
[ASCII Art Archive](https://www.asciiart.eu/)<br/>

